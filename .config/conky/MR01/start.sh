#!/bin/bash

sleep 20
DIR=~/.config/conky/MR01/


launch() {
    RUN=`ps aux | grep conky | grep $1`

    if [ -z "$RUN" ]; then
        conky -c $DIR$1 -q &
    fi
}

launch "calrc"
launch "systemrc"
launch "cpurc"
launch "memoryrc"
launch "diskrc"
launch "networkrc"
launch "clockrc"

-- Based on : https://www.deviantart.com/sg-cyrax/art/Lua-Calendar-for-Conky-203708623
-- calendar.lua
-- Calendar for Conky
--

months = {
-- Add translations for names of months for your language
           en = { "JANUARY", "FEBRUARY", "MARCH", "APRIL",
                  "MAY", "JUNE", "JULY", "AUGUST",
                  "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER" },
           fr = { "JANVIER", "FEVRIER", "MARS", "AVRIL",
                  "MAI", "JUIN", "JUILLET", "AOUT",
                  "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DECEMBRE" },
           ru = { "ЯНВАРЬ", "ФЕВРАЛЬ", "МАРТ", "АПРЕЛЬ",
                  "МАЙ", "ИЮНЬ", "ИЮЛЬ", "АВГУСТ",
                  "СЕНТЯБРЬ", "ОКТЯБРЬ", "НОЯБРЬ", "ДЕКАБРЬ" }
         }
weekDays = {
-- Add translations for acronyms names days of the week for your language
             en = { "SU", "MO", "TU", "WE", "TH", "FR", "SA" },
             fr = { "DI", "LU", "MA", "ME", "JE", "VE", "SA" },
             ru = { "ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ" }
           }
weekDaysFull = {
-- Add translations for names days of the week for your language
                 en = { "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY",
                        "THURSDAY", "FRIDAY", "SATURDAY" },
                 fr = { "DIMANCHE", "LUNDI", "MARDI", "MERCREDI",
                        "JEUDI", "VENDREDI", "SAMEDI" },
                 ru = { "ВОСКРЕСЕНЬЕ", "ПОНЕДЕЛЬНИК", "ВТОРНИК", "СРЕДА",
                        "ЧЕТВЕРГ", "ПЯТНИЦА", "СУББОТА" }
               }

function conky_calendar ()
	local t = { lang          = "fr", -- Language for display Months and Days of week
                                    -- Change it to your language, and add translations
                                    -- for into the tables named 'months', 'weekDays'
                                    -- and 'weekDaseFull' at top of this file
	            right_space   = 100,
	            left_space    = 220,
	            back_voffset  = -140,
	            draw_spin     = false,
	            normal_font   = "Hack Nerd Font:pixelsize=15",
	            big_font      = "iosevka:style=Bold:pixelsize=72",
	            grid_font     = "Hack Nerd Font:pixelsize=15",
	            today_color   = "A4BCDC",
	            weekend_color = "7192D3",
	            spin_color    = "5C8599", }
	local m, y = os.date("%m"), os.date("%Y")
	return displayCalendar(m, y, 2, t) -- Month, Year, Start of Week (1 = Sunday, 2 = Monday)
	                                   -- and parameters table :-)
end

-- Display the calendar
-- Based on some examples from this page: http://lua-users.org/wiki/DisplayCalendarInHtml
function displayCalendar(month, year, weekStart, params)
	local t, wkSt = os.time{year=year, month=month+1, day=0}, weekStart or 1
	local d = os.date("*t", t)
	local mthDays, stDay = d.day, (d.wday - d.day - wkSt+1) % 7
	local wd = tonumber(os.date("%w")) + 1
	local mths = months[params.lang] or months["en"]
	local wdsf = weekDaysFull[params.lang] or weekDaysFull["en"]
	local wds = weekDays[params.lang] or weekDays["en"]

	local rs = "${alignc " .. params.right_space .. "}"
	local ls = "${goto " .. params.left_space .. "}"
	local bv = "${voffset " .. params.back_voffset .. "}"
	local df = "${font}"
	local nf = "${font " .. params.normal_font .. "}"
	local bf = "${font " .. params.big_font .. "}"
	local gf = "${font " .. params.grid_font .. "}"
	local wc = "${color " .. params.weekend_color .. "}"
	local sc = "${color " .. params.spin_color .. "}"
	local toc = "${color " .. params.today_color .. "}"
	

	local result = "\n\n"


	if wd == 1 or wd == 7 then
		result = result .. nf .. rs ..toc ..  wdsf[wd] .. "${color}" .. df .. "\n"
		result = result .. bf .. rs .. toc .. os.date("%d") .. "${color}" .. df .. "\n\n${voffset -2}"
	else
		result = result .. nf .. rs .. wdsf[wd] .. df .. "\n"
		result = result .. bf .. rs .. os.date("%d") .. df .. "\n\n${voffset -2}"
	end
	
	result = result .. nf .. rs .. toc .. mths[tonumber(month)] .. df .. "${color}\n\n${voffset -10}"

	if params.draw_spin then
		result = result .. bv .. sc .. "${voffset -15}${goto 1}${hr 15}${color}\n\n${voffset -13}"
	else
		result = result .. bv .. "${voffset 2}"
	end

	result = result .. ls .. gf

	for x=0,6 do
		wd = (x + wkSt) <= 7 and x + wkSt or 1

		if wd == 1 or wd == 7 then
			result = result .. wc
		end

		result = result .. wds[wd] .. " "

		if wd == 1 or wd == 7 then
			result = result .. "${color}"
		end
	end

	result = result .. "\n\n${voffset -13}" .. ls .. string.rep("   ", stDay)

	for x = 1, mthDays do
		wd = os.date("%w", os.time{year = year, month = month, day = x}) + 1

		if wd == 1 or wd == 7 then
			result = result .. wc
		end

		if x < 10 then
			result = result .. "0"
		end

		result = result .. x .. " "

		if wd == 1 or wd == 7 then
			result = result .. "${color}"
		end

		if (x+stDay)%7==0 then
			result = result .. "\n\n${voffset -14}" .. ls
		end
	end

	return result
end
	


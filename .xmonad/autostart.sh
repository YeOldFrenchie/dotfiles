#!/bin/bash

#-- Ye Old Frenchie - https://gitlab.com/YeOldFrenchie
#-- xmonad starting script for my Latitude E6540
#-- Original from ArcolinuxB Xmonad Distro

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}


(sleep 2; run $HOME/.config/polybar/launch.sh) &


#cursor active at boot
xsetroot -cursor_name left_ptr &

#starting utility applications at boot time
run nm-applet &
run pamac-tray &
run xfce4-power-manager &
run volumeicon &
run numlockx on &
run blueberry-tray &
run picom --config $HOME/.config/picom/picom.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &

#starting user applications at boot time
run nitrogen --restore &
run xscreensaver -no-splash &
/home/maximilien/.config/conky/MR01/start.sh
